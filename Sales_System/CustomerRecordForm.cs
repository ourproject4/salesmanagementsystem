﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sales_System
{
    public partial class CustomerRecordForm : MetroFramework.Forms.MetroForm
    {
        public CustomerRecordForm()
        {
            InitializeComponent();
        }

        private void CustomerRecordForm_Load(object sender, EventArgs e)
        {
            LoadAllCustomersData();
        }

        private void LoadAllCustomersData()
        {
            CustomersDataGridView.DataSource = GetData();
            CustomersDataGridView.Columns[0].Visible = false;

            if (CustomersDataGridView.Columns.Contains("Delete"))
            {
                CustomersDataGridView.Columns.Remove("Delete");
            }

            CustomersDataGridView.DataSource = GetData();
            CustomersDataGridView.Columns[0].Visible = true;

            DataGridViewButtonColumn dvc = new DataGridViewButtonColumn
            {
                HeaderText = "Delete",
                Name = "Delete"
            };
            CustomersDataGridView.Columns.Add(dvc);
        }

        private DataTable GetData()
        {
            DataTable dtdata = new DataTable();
            using (SqlConnection con = new SqlConnection(ApplicationSetting.ConnectionString()))
            {
                using (SqlCommand cmd = new SqlCommand("Select * from Customers", con))
                {
                    con.Open();
                    SqlDataReader sdr = cmd.ExecuteReader();
                    dtdata.Load(sdr);
                }
            }
            return dtdata;
        }

        private void CusNameTextBox_TextChanged(object sender, EventArgs e)
        {
            if (CusNameTextBox.Text.Trim() == string.Empty)
            {
                LoadAllCustomersData();
            }
            else
            {
                CustomersDataGridView.DataSource = SerachCustomerDetailByCustomerName();
                CustomersDataGridView.Columns[0].Visible = true;
            }
        }

        private DataTable SerachCustomerDetailByCustomerName()
        {
            DataTable dtdata = new DataTable();
            using (SqlConnection con = new SqlConnection(ApplicationSetting.ConnectionString()))
            {
                using (SqlCommand cmd = new SqlCommand("Select * from Customers WHERE Name Like+'%' +@Name +'%'", con))
                {
                    cmd.Parameters.AddWithValue("@Name", CusNameTextBox.Text);
                    con.Open();
                    SqlDataReader sdr = cmd.ExecuteReader();
                    dtdata.Load(sdr);
                }
            }
            return dtdata;
        }

        private void CustomersDataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (CustomersDataGridView.Columns[e.ColumnIndex].HeaderText == "Delete")
            {
                DialogResult confirm = MessageBox.Show("Are you sure you want to delete", "Message", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (confirm == DialogResult.Yes)
                {
                    int CustomerId;
                    CustomerId = Convert.ToInt32(CustomersDataGridView.Rows[e.RowIndex].Cells["CustomerId"].Value);
                    using (SqlConnection con = new SqlConnection(ApplicationSetting.ConnectionString()))
                    {
                        con.Open();
                        try
                        {
                            using (SqlCommand cmd = new SqlCommand("DELETE FROM Customers WHERE CustomerId=@code", con))
                            {
                                cmd.Parameters.AddWithValue("@code", CustomerId);
                                int result = cmd.ExecuteNonQuery();
                                if (result > 0)
                                {
                                    MessageBox.Show("Data Deleted Successfully");
                                }
                                else
                                {
                                    MessageBox.Show("Data not deleted");
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                        finally
                        {
                            con.Close();
                        }

                    }
                }
            }
        }

        private void Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
   