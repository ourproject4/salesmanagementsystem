﻿
namespace Sales_System
{
    partial class Dashboardforms
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Dashboardforms));
            this.panel1 = new System.Windows.Forms.Panel();
            this.AddproButton1 = new Guna.UI2.WinForms.Guna2ImageButton();
            this.StockManButton = new Guna.UI2.WinForms.Guna2ImageButton();
            this.NewCustButton = new Guna.UI2.WinForms.Guna2ImageButton();
            this.ManProButton = new Guna.UI2.WinForms.Guna2ImageButton();
            this.MetroBtn = new Guna.UI2.WinForms.Guna2ImageButton();
            this.label1 = new System.Windows.Forms.Label();
            this.AddSalesButton = new Guna.UI2.WinForms.Guna2ImageButton();
            this.Exit = new Guna.UI2.WinForms.Guna2ImageButton();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Red;
            this.panel1.Location = new System.Drawing.Point(23, 542);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(989, 10);
            this.panel1.TabIndex = 12;
            // 
            // AddproButton1
            // 
            this.AddproButton1.CheckedState.ImageSize = new System.Drawing.Size(64, 64);
            this.AddproButton1.CheckedState.Parent = this.AddproButton1;
            this.AddproButton1.HoverState.ImageSize = new System.Drawing.Size(64, 64);
            this.AddproButton1.HoverState.Parent = this.AddproButton1;
            this.AddproButton1.Image = ((System.Drawing.Image)(resources.GetObject("AddproButton1.Image")));
            this.AddproButton1.ImageRotate = 0F;
            this.AddproButton1.ImageSize = new System.Drawing.Size(120, 120);
            this.AddproButton1.Location = new System.Drawing.Point(71, 80);
            this.AddproButton1.Name = "AddproButton1";
            this.AddproButton1.PressedState.ImageSize = new System.Drawing.Size(64, 64);
            this.AddproButton1.PressedState.Parent = this.AddproButton1;
            this.AddproButton1.Size = new System.Drawing.Size(132, 138);
            this.AddproButton1.TabIndex = 13;
            this.AddproButton1.Click += new System.EventHandler(this.AddproButton1_Click);
            // 
            // StockManButton
            // 
            this.StockManButton.CheckedState.ImageSize = new System.Drawing.Size(64, 64);
            this.StockManButton.CheckedState.Parent = this.StockManButton;
            this.StockManButton.HoverState.ImageSize = new System.Drawing.Size(64, 64);
            this.StockManButton.HoverState.Parent = this.StockManButton;
            this.StockManButton.Image = ((System.Drawing.Image)(resources.GetObject("StockManButton.Image")));
            this.StockManButton.ImageRotate = 0F;
            this.StockManButton.ImageSize = new System.Drawing.Size(120, 120);
            this.StockManButton.Location = new System.Drawing.Point(797, 92);
            this.StockManButton.Name = "StockManButton";
            this.StockManButton.PressedState.ImageSize = new System.Drawing.Size(64, 64);
            this.StockManButton.PressedState.Parent = this.StockManButton;
            this.StockManButton.Size = new System.Drawing.Size(132, 138);
            this.StockManButton.TabIndex = 14;
            this.StockManButton.Click += new System.EventHandler(this.StockManButton_Click_1);
            // 
            // NewCustButton
            // 
            this.NewCustButton.CheckedState.ImageSize = new System.Drawing.Size(64, 64);
            this.NewCustButton.CheckedState.Parent = this.NewCustButton;
            this.NewCustButton.HoverState.ImageSize = new System.Drawing.Size(64, 64);
            this.NewCustButton.HoverState.Parent = this.NewCustButton;
            this.NewCustButton.Image = ((System.Drawing.Image)(resources.GetObject("NewCustButton.Image")));
            this.NewCustButton.ImageRotate = 0F;
            this.NewCustButton.ImageSize = new System.Drawing.Size(120, 120);
            this.NewCustButton.Location = new System.Drawing.Point(190, 308);
            this.NewCustButton.Name = "NewCustButton";
            this.NewCustButton.PressedState.ImageSize = new System.Drawing.Size(64, 64);
            this.NewCustButton.PressedState.Parent = this.NewCustButton;
            this.NewCustButton.Size = new System.Drawing.Size(132, 138);
            this.NewCustButton.TabIndex = 15;
            this.NewCustButton.Click += new System.EventHandler(this.NewCustButton_Click_1);
            // 
            // ManProButton
            // 
            this.ManProButton.CheckedState.ImageSize = new System.Drawing.Size(64, 64);
            this.ManProButton.CheckedState.Parent = this.ManProButton;
            this.ManProButton.HoverState.ImageSize = new System.Drawing.Size(64, 64);
            this.ManProButton.HoverState.Parent = this.ManProButton;
            this.ManProButton.Image = ((System.Drawing.Image)(resources.GetObject("ManProButton.Image")));
            this.ManProButton.ImageRotate = 0F;
            this.ManProButton.ImageSize = new System.Drawing.Size(120, 120);
            this.ManProButton.Location = new System.Drawing.Point(309, 80);
            this.ManProButton.Name = "ManProButton";
            this.ManProButton.PressedState.ImageSize = new System.Drawing.Size(64, 64);
            this.ManProButton.PressedState.Parent = this.ManProButton;
            this.ManProButton.Size = new System.Drawing.Size(132, 138);
            this.ManProButton.TabIndex = 16;
            this.ManProButton.Click += new System.EventHandler(this.ManProButton_Click_1);
            // 
            // MetroBtn
            // 
            this.MetroBtn.CheckedState.ImageSize = new System.Drawing.Size(64, 64);
            this.MetroBtn.CheckedState.Parent = this.MetroBtn;
            this.MetroBtn.HoverState.ImageSize = new System.Drawing.Size(64, 64);
            this.MetroBtn.HoverState.Parent = this.MetroBtn;
            this.MetroBtn.Image = ((System.Drawing.Image)(resources.GetObject("MetroBtn.Image")));
            this.MetroBtn.ImageRotate = 0F;
            this.MetroBtn.ImageSize = new System.Drawing.Size(120, 120);
            this.MetroBtn.Location = new System.Drawing.Point(460, 308);
            this.MetroBtn.Name = "MetroBtn";
            this.MetroBtn.PressedState.ImageSize = new System.Drawing.Size(64, 64);
            this.MetroBtn.PressedState.Parent = this.MetroBtn;
            this.MetroBtn.Size = new System.Drawing.Size(132, 138);
            this.MetroBtn.TabIndex = 18;
            this.MetroBtn.Click += new System.EventHandler(this.MetroBtn_Click_1);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(84, 221);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(119, 23);
            this.label1.TabIndex = 19;
            this.label1.Text = "Add Products";
            // 
            // AddSalesButton
            // 
            this.AddSalesButton.CheckedState.ImageSize = new System.Drawing.Size(64, 64);
            this.AddSalesButton.CheckedState.Parent = this.AddSalesButton;
            this.AddSalesButton.HoverState.ImageSize = new System.Drawing.Size(64, 64);
            this.AddSalesButton.HoverState.Parent = this.AddSalesButton;
            this.AddSalesButton.Image = ((System.Drawing.Image)(resources.GetObject("AddSalesButton.Image")));
            this.AddSalesButton.ImageRotate = 0F;
            this.AddSalesButton.ImageSize = new System.Drawing.Size(120, 120);
            this.AddSalesButton.Location = new System.Drawing.Point(544, 80);
            this.AddSalesButton.Name = "AddSalesButton";
            this.AddSalesButton.PressedState.ImageSize = new System.Drawing.Size(64, 64);
            this.AddSalesButton.PressedState.Parent = this.AddSalesButton;
            this.AddSalesButton.Size = new System.Drawing.Size(132, 138);
            this.AddSalesButton.TabIndex = 20;
            this.AddSalesButton.Click += new System.EventHandler(this.AddSalesButton_Click_1);
            // 
            // Exit
            // 
            this.Exit.CheckedState.ImageSize = new System.Drawing.Size(64, 64);
            this.Exit.CheckedState.Parent = this.Exit;
            this.Exit.HoverState.ImageSize = new System.Drawing.Size(64, 64);
            this.Exit.HoverState.Parent = this.Exit;
            this.Exit.Image = ((System.Drawing.Image)(resources.GetObject("Exit.Image")));
            this.Exit.ImageRotate = 0F;
            this.Exit.ImageSize = new System.Drawing.Size(120, 120);
            this.Exit.Location = new System.Drawing.Point(725, 308);
            this.Exit.Name = "Exit";
            this.Exit.PressedState.ImageSize = new System.Drawing.Size(64, 64);
            this.Exit.PressedState.Parent = this.Exit;
            this.Exit.Size = new System.Drawing.Size(138, 138);
            this.Exit.TabIndex = 21;
            this.Exit.Click += new System.EventHandler(this.Exit_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(305, 221);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(148, 23);
            this.label2.TabIndex = 22;
            this.label2.Text = "Products Records";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(793, 221);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(166, 23);
            this.label3.TabIndex = 23;
            this.label3.Text = "Stock Management";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(322, 395);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(0, 23);
            this.label4.TabIndex = 24;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(186, 459);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(133, 23);
            this.label5.TabIndex = 25;
            this.label5.Text = "Add Customers";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(456, 459);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(155, 23);
            this.label6.TabIndex = 26;
            this.label6.Text = "Customer Records";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(540, 221);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(141, 23);
            this.label7.TabIndex = 27;
            this.label7.Text = "Add Sales Order";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(780, 459);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(45, 23);
            this.label8.TabIndex = 28;
            this.label8.Text = "Exit ";
            // 
            // Dashboardforms
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1028, 575);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Exit);
            this.Controls.Add(this.AddSalesButton);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.MetroBtn);
            this.Controls.Add(this.ManProButton);
            this.Controls.Add(this.NewCustButton);
            this.Controls.Add(this.StockManButton);
            this.Controls.Add(this.AddproButton1);
            this.Controls.Add(this.panel1);
            this.Name = "Dashboardforms";
            this.Style = MetroFramework.MetroColorStyle.Orange;
            this.Text = "Dashboard";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private Guna.UI2.WinForms.Guna2ImageButton AddproButton1;
        private Guna.UI2.WinForms.Guna2ImageButton StockManButton;
        private Guna.UI2.WinForms.Guna2ImageButton NewCustButton;
        private Guna.UI2.WinForms.Guna2ImageButton ManProButton;
        private Guna.UI2.WinForms.Guna2ImageButton MetroBtn;
        private System.Windows.Forms.Label label1;
        private Guna.UI2.WinForms.Guna2ImageButton AddSalesButton;
        private Guna.UI2.WinForms.Guna2ImageButton Exit;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
    }
}