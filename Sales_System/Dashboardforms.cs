﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sales_System
{
    public partial class Dashboardforms : MetroFramework.Forms.MetroForm
    {
        public Dashboardforms()
        {
            InitializeComponent();
        }

       

        private void NewCustButton_Click_1(object sender, EventArgs e)
        {
            NewCustomerForm ncf = new NewCustomerForm();
            ncf.Show();
        }

        private void ManProButton_Click_1(object sender, EventArgs e)
        {
            ProductsRecordsForm prd = new ProductsRecordsForm();
            prd.Show();
        }

        private void AddSalesButton_Click_1(object sender, EventArgs e)
        {
            SalesOrderDetails aods = new SalesOrderDetails();
            aods.Show();
        }

        private void StockManButton_Click_1(object sender, EventArgs e)
        {
            Stock_Management sm = new Stock_Management();
            sm.Show();
        }

        private void MetroBtn_Click_1(object sender, EventArgs e)
        {
            CustomerRecordForm crf = new CustomerRecordForm();
            crf.Show();
        }

        private void Exit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void AddproButton1_Click(object sender, EventArgs e)
        {
            DefineProducts dps = new DefineProducts();
            dps.Show();
        }
    }
    
}
