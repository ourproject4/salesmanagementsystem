﻿
namespace Sales_System
{
    partial class DefineProducts
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.SizesdataGridView = new System.Windows.Forms.DataGridView();
            this.SalesPriceTextBox = new System.Windows.Forms.TextBox();
            this.PurchaseTextBox = new System.Windows.Forms.TextBox();
            this.ProductTextBox = new System.Windows.Forms.TextBox();
            this.CategoryComboBox = new System.Windows.Forms.ComboBox();
            this.SupplierComboBox = new System.Windows.Forms.ComboBox();
            this.DeNewBtn = new Guna.UI2.WinForms.Guna2Button();
            this.Save = new Guna.UI2.WinForms.Guna2Button();
            this.Close = new Guna.UI2.WinForms.Guna2Button();
            ((System.ComponentModel.ISupportInitialize)(this.SizesdataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe UI Symbol", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(81, 81);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(134, 23);
            this.label6.TabIndex = 29;
            this.label6.Text = "Broadcast Size";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI Symbol", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(617, 412);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(101, 23);
            this.label5.TabIndex = 28;
            this.label5.Text = "Sales Price";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI Symbol", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(604, 339);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(135, 23);
            this.label4.TabIndex = 27;
            this.label4.Text = "Purchase Price";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI Symbol", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(604, 267);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(138, 23);
            this.label3.TabIndex = 26;
            this.label3.Text = "Products Seller";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI Symbol", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(600, 198);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(160, 23);
            this.label2.TabIndex = 25;
            this.label2.Text = "Product Category";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI Symbol", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(604, 128);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(133, 23);
            this.label1.TabIndex = 24;
            this.label1.Text = "Product Name";
            // 
            // SizesdataGridView
            // 
            this.SizesdataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.SizesdataGridView.DefaultCellStyle = dataGridViewCellStyle1;
            this.SizesdataGridView.Location = new System.Drawing.Point(40, 121);
            this.SizesdataGridView.Name = "SizesdataGridView";
            this.SizesdataGridView.RowHeadersWidth = 51;
            this.SizesdataGridView.RowTemplate.Height = 24;
            this.SizesdataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.SizesdataGridView.Size = new System.Drawing.Size(254, 324);
            this.SizesdataGridView.TabIndex = 23;
            // 
            // SalesPriceTextBox
            // 
            this.SalesPriceTextBox.Font = new System.Drawing.Font("Segoe UI Emoji", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SalesPriceTextBox.Location = new System.Drawing.Point(402, 412);
            this.SalesPriceTextBox.Multiline = true;
            this.SalesPriceTextBox.Name = "SalesPriceTextBox";
            this.SalesPriceTextBox.Size = new System.Drawing.Size(169, 31);
            this.SalesPriceTextBox.TabIndex = 22;
            // 
            // PurchaseTextBox
            // 
            this.PurchaseTextBox.Font = new System.Drawing.Font("Segoe UI Emoji", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PurchaseTextBox.Location = new System.Drawing.Point(402, 336);
            this.PurchaseTextBox.Multiline = true;
            this.PurchaseTextBox.Name = "PurchaseTextBox";
            this.PurchaseTextBox.Size = new System.Drawing.Size(169, 34);
            this.PurchaseTextBox.TabIndex = 21;
            // 
            // ProductTextBox
            // 
            this.ProductTextBox.Font = new System.Drawing.Font("Segoe UI Symbol", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ProductTextBox.Location = new System.Drawing.Point(311, 121);
            this.ProductTextBox.Multiline = true;
            this.ProductTextBox.Name = "ProductTextBox";
            this.ProductTextBox.Size = new System.Drawing.Size(264, 28);
            this.ProductTextBox.TabIndex = 20;
            // 
            // CategoryComboBox
            // 
            this.CategoryComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CategoryComboBox.FormattingEnabled = true;
            this.CategoryComboBox.Location = new System.Drawing.Point(353, 196);
            this.CategoryComboBox.Name = "CategoryComboBox";
            this.CategoryComboBox.Size = new System.Drawing.Size(222, 30);
            this.CategoryComboBox.TabIndex = 30;
            // 
            // SupplierComboBox
            // 
            this.SupplierComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SupplierComboBox.FormattingEnabled = true;
            this.SupplierComboBox.Location = new System.Drawing.Point(353, 266);
            this.SupplierComboBox.Name = "SupplierComboBox";
            this.SupplierComboBox.Size = new System.Drawing.Size(222, 30);
            this.SupplierComboBox.TabIndex = 31;
            // 
            // DeNewBtn
            // 
            this.DeNewBtn.BorderColor = System.Drawing.Color.MidnightBlue;
            this.DeNewBtn.BorderRadius = 18;
            this.DeNewBtn.BorderThickness = 2;
            this.DeNewBtn.CheckedState.Parent = this.DeNewBtn;
            this.DeNewBtn.CustomImages.Parent = this.DeNewBtn;
            this.DeNewBtn.FillColor = System.Drawing.Color.Navy;
            this.DeNewBtn.Font = new System.Drawing.Font("Segoe UI Emoji", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DeNewBtn.ForeColor = System.Drawing.Color.White;
            this.DeNewBtn.HoverState.Parent = this.DeNewBtn;
            this.DeNewBtn.Location = new System.Drawing.Point(103, 498);
            this.DeNewBtn.Name = "DeNewBtn";
            this.DeNewBtn.ShadowDecoration.Parent = this.DeNewBtn;
            this.DeNewBtn.Size = new System.Drawing.Size(163, 42);
            this.DeNewBtn.TabIndex = 32;
            this.DeNewBtn.Text = "DEFINE NEW";
            this.DeNewBtn.Click += new System.EventHandler(this.DeNewBtn_Click);
            // 
            // Save
            // 
            this.Save.BorderColor = System.Drawing.Color.MidnightBlue;
            this.Save.BorderRadius = 18;
            this.Save.BorderThickness = 2;
            this.Save.CheckedState.Parent = this.Save;
            this.Save.CustomImages.Parent = this.Save;
            this.Save.FillColor = System.Drawing.Color.Navy;
            this.Save.Font = new System.Drawing.Font("Segoe UI Emoji", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Save.ForeColor = System.Drawing.Color.White;
            this.Save.HoverState.Parent = this.Save;
            this.Save.Location = new System.Drawing.Point(311, 498);
            this.Save.Name = "Save";
            this.Save.ShadowDecoration.Parent = this.Save;
            this.Save.Size = new System.Drawing.Size(134, 42);
            this.Save.TabIndex = 33;
            this.Save.Text = "SAVE";
            this.Save.Click += new System.EventHandler(this.Save_Click);
            // 
            // Close
            // 
            this.Close.BorderColor = System.Drawing.Color.MidnightBlue;
            this.Close.BorderRadius = 18;
            this.Close.BorderThickness = 2;
            this.Close.CheckedState.Parent = this.Close;
            this.Close.CustomImages.Parent = this.Close;
            this.Close.FillColor = System.Drawing.Color.Navy;
            this.Close.Font = new System.Drawing.Font("Segoe UI Emoji", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Close.ForeColor = System.Drawing.Color.White;
            this.Close.HoverState.Parent = this.Close;
            this.Close.Location = new System.Drawing.Point(498, 498);
            this.Close.Name = "Close";
            this.Close.ShadowDecoration.Parent = this.Close;
            this.Close.Size = new System.Drawing.Size(143, 42);
            this.Close.TabIndex = 34;
            this.Close.Text = "CLOSE";
            this.Close.Click += new System.EventHandler(this.Close_Click);
            // 
            // DefineProducts
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 574);
            this.Controls.Add(this.Close);
            this.Controls.Add(this.Save);
            this.Controls.Add(this.DeNewBtn);
            this.Controls.Add(this.SupplierComboBox);
            this.Controls.Add(this.CategoryComboBox);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.SizesdataGridView);
            this.Controls.Add(this.SalesPriceTextBox);
            this.Controls.Add(this.PurchaseTextBox);
            this.Controls.Add(this.ProductTextBox);
            this.Name = "DefineProducts";
            this.Text = "DefineProducts";
            this.Load += new System.EventHandler(this.DefineProducts_Load);
            ((System.ComponentModel.ISupportInitialize)(this.SizesdataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView SizesdataGridView;
        private System.Windows.Forms.TextBox SalesPriceTextBox;
        private System.Windows.Forms.TextBox PurchaseTextBox;
        private System.Windows.Forms.TextBox ProductTextBox;
        private System.Windows.Forms.ComboBox CategoryComboBox;
        private System.Windows.Forms.ComboBox SupplierComboBox;
        private Guna.UI2.WinForms.Guna2Button DeNewBtn;
        private Guna.UI2.WinForms.Guna2Button Save;
        private Guna.UI2.WinForms.Guna2Button Close;
    }
}