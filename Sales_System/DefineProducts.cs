﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sales_System
{
    public partial class DefineProducts : MetroFramework.Forms.MetroForm
    {

        public DefineProducts()
        {
            InitializeComponent();
        }
        public bool IsUpdate { get; set; }
        public object listTypeID { get; private set; }

        private readonly List<int> SizesCart = new List<int>();
        

        private void DefineProducts_Load(object sender, EventArgs e)
        {
            if (!this.IsUpdate)
            {

            }
            LoadAllSizesinDataGridView();
            LoadDataIntoComboBoxes();
        }

        private void LoadAllSizesinDataGridView()
        {
            SizesdataGridView.DataSource = GetSizesData();
            SizesdataGridView.Columns[0].Visible = false;
        }

        private DataTable GetSizesData()
        {
            DataTable dtsizes = new DataTable();

            using (SqlConnection con = new SqlConnection(ApplicationSetting.ConnectionString()))
            {
                using (SqlCommand cmd = new SqlCommand("usp_Sizes_LoadAllSizes", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ListTypeID", 1);
                    con.Open();
                    SqlDataReader sdr = cmd.ExecuteReader();
                    dtsizes.Load(sdr);
                }
            }
            return dtsizes;
        }

        private void LoadDataIntoComboBoxes()
        {
            CategoryComboBox.DataSource = GetComboBoxData(3);
            CategoryComboBox.DisplayMember = "Description";
            CategoryComboBox.ValueMember = "ID";

            SupplierComboBox.DataSource = GetComboBoxData(2);
            SupplierComboBox.DisplayMember = "Description";
            SupplierComboBox.ValueMember = "ID";
        }

        private DataTable GetComboBoxData(int listTypeID)
        {
            DataTable dtrecord = new DataTable();

            using (SqlConnection con = new SqlConnection(ApplicationSetting.ConnectionString()))
            {
                using (SqlCommand cmd = new SqlCommand("usp_ListTypeData_LoadDataintoComboboxes", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ListTypeID", listTypeID);
                    con.Open();
                    SqlDataReader sdr = cmd.ExecuteReader();
                    dtrecord.Load(sdr);
                }
            }
            return dtrecord;
        }

        private void DeNewBtn_Click(object sender, EventArgs e)
        {
            ClearControls();
        }

        private void ClearControls()
        {

            ProductTextBox.Clear();
            PurchaseTextBox.Clear();
            SalesPriceTextBox.Clear();
            SupplierComboBox.SelectedIndex = -1;
            CategoryComboBox.SelectedIndex = -1;


            foreach (DataGridViewRow Row in SizesdataGridView.Rows)
            {
                Row.Cells["Select"].Value = 0;
            }
            ProductTextBox.Focus();
        }

        private void Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Save_Click(object sender, EventArgs e)
        {
            if (IsValid())
            {
                SizesCart.Clear();
                LoadDataIntoSizesCart();
                using (SqlConnection con = new SqlConnection(ApplicationSetting.ConnectionString()))
                {
                    using (SqlCommand cmd = new SqlCommand("usp_Products_InsertNewProducts", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ProductName", ProductTextBox.Text);
                        cmd.Parameters.AddWithValue("@CategoryID", CategoryComboBox.SelectedValue);
                        cmd.Parameters.AddWithValue("@SupplierID", SupplierComboBox.SelectedValue);
                        cmd.Parameters.AddWithValue("@PurchasePrice", PurchaseTextBox.Text);
                        cmd.Parameters.AddWithValue("@SalesPrice", SalesPriceTextBox.Text);
                        con.Open();
                        int id = Convert.ToInt32(cmd.ExecuteScalar());
                        SaveSizes(id);
                        MessageBox.Show("Product No." + id.ToString() + " is saved successfully in the system ", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    }
                }
            }
        }

        private void LoadDataIntoSizesCart()
        {
            foreach (DataGridViewRow row in SizesdataGridView.Rows)
            {
                if (row.Cells["Select"].Value != null && !DBNull.Value.Equals(row.Cells["Select"].Value) && Convert.ToBoolean(row.Cells["Select"].Value) == true)
                {
                    SizesCart.Add((int)row.Cells["ID"].Value);
                }
            }
        }

        private bool IsValid()
        {
            {
                if (ProductTextBox.Text.Trim() == string.Empty)
                {
                    MessageBox.Show("Product Name is required", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    ProductTextBox.Focus();
                    return false;
                }
                if (PurchaseTextBox.Text.Trim() == string.Empty)
                {
                    MessageBox.Show("Purchase price is required", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    PurchaseTextBox.Focus();
                    return false;
                }
                return true;
            }
        }

        private void SaveSizes(int _id)
        {
            using (SqlConnection con = new SqlConnection(ApplicationSetting.ConnectionString()))
            {
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }

                foreach (int sizeID in SizesCart)
                {
                    using (SqlCommand cmd = new SqlCommand("usp_Products_InsertIntoproductSizes", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ProductID", _id);
                        cmd.Parameters.AddWithValue("@SizeID", sizeID);
                        SaveSizes(sizeID);
                        cmd.ExecuteNonQuery();

                    }
                }
            }
        }
    }
    
}
