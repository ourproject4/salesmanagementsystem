﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace Sales_System
{
    public partial class Login : MetroFramework.Forms.MetroForm
    {

        public Login()
        {
            InitializeComponent();
        }

        private void LoginButton_Click(object sender, EventArgs e)
        {
            if (GetIsValid())
            {
                using (SqlConnection con = new SqlConnection(ApplicationSetting.ConnectionString()))
                {
                    using (SqlCommand cmd = new SqlCommand("usp_Login_VerifyLoginDetails", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@Username", UserTextBox.Text.Trim());
                        cmd.Parameters.AddWithValue("@Password", PwdTextBox.Text.Trim());
                        con.Open();
                        SqlDataReader sdr = cmd.ExecuteReader();
                        if (sdr.Read())
                        {
                            this.Hide();
                            Dashboardforms ds = new Dashboardforms();
                            ds.Show();
                        }
                        else
                        {
                            MessageBox.Show("User name or password is incorrect", "Login failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }
            }
        }

        private bool GetIsValid()
        {
            if (UserTextBox.Text.Trim() == string.Empty)
            {
                MessageBox.Show("User name is required", "form validation error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                UserTextBox.Focus();
                return false;
            }
            if (PwdTextBox.Text.Trim() == string.Empty)
            {
                MessageBox.Show("Password is required", "form validation error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                PwdTextBox.Focus();
                return false;
            }
            return true;

        }

        private void ExitButton_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
    
}
