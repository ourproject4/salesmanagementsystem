﻿
namespace Sales_System
{
    partial class NewCustomerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.AddressTextBox = new System.Windows.Forms.TextBox();
            this.MobileTextBox = new System.Windows.Forms.TextBox();
            this.CustomerTextBox = new System.Windows.Forms.TextBox();
            this.LoginButton = new Guna.UI2.WinForms.Guna2Button();
            this.NewBtn = new Guna.UI2.WinForms.Guna2Button();
            this.Closebtn = new Guna.UI2.WinForms.Guna2Button();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI Emoji", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(28, 183);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(181, 26);
            this.label2.TabIndex = 30;
            this.label2.Text = "Customer Address";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI Emoji", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(125, 318);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 26);
            this.label4.TabIndex = 29;
            this.label4.Text = "Mobile";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI Emoji", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(45, 124);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(161, 26);
            this.label1.TabIndex = 28;
            this.label1.Text = "Customer Name";
            // 
            // AddressTextBox
            // 
            this.AddressTextBox.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AddressTextBox.Location = new System.Drawing.Point(226, 170);
            this.AddressTextBox.Multiline = true;
            this.AddressTextBox.Name = "AddressTextBox";
            this.AddressTextBox.Size = new System.Drawing.Size(296, 129);
            this.AddressTextBox.TabIndex = 27;
            // 
            // MobileTextBox
            // 
            this.MobileTextBox.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MobileTextBox.Location = new System.Drawing.Point(226, 318);
            this.MobileTextBox.Multiline = true;
            this.MobileTextBox.Name = "MobileTextBox";
            this.MobileTextBox.Size = new System.Drawing.Size(234, 33);
            this.MobileTextBox.TabIndex = 26;
            // 
            // CustomerTextBox
            // 
            this.CustomerTextBox.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CustomerTextBox.Location = new System.Drawing.Point(226, 119);
            this.CustomerTextBox.Multiline = true;
            this.CustomerTextBox.Name = "CustomerTextBox";
            this.CustomerTextBox.Size = new System.Drawing.Size(332, 28);
            this.CustomerTextBox.TabIndex = 25;
            // 
            // LoginButton
            // 
            this.LoginButton.BorderColor = System.Drawing.Color.DarkBlue;
            this.LoginButton.BorderRadius = 15;
            this.LoginButton.CheckedState.Parent = this.LoginButton;
            this.LoginButton.CustomImages.Parent = this.LoginButton;
            this.LoginButton.FillColor = System.Drawing.Color.Navy;
            this.LoginButton.Font = new System.Drawing.Font("Segoe UI Symbol", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LoginButton.ForeColor = System.Drawing.Color.White;
            this.LoginButton.HoverState.Parent = this.LoginButton;
            this.LoginButton.Location = new System.Drawing.Point(226, 434);
            this.LoginButton.Name = "LoginButton";
            this.LoginButton.ShadowDecoration.Parent = this.LoginButton;
            this.LoginButton.Size = new System.Drawing.Size(332, 39);
            this.LoginButton.TabIndex = 31;
            this.LoginButton.Text = "SAVE";
            this.LoginButton.Click += new System.EventHandler(this.LoginButton_Click);
            // 
            // NewBtn
            // 
            this.NewBtn.BorderColor = System.Drawing.Color.DarkBlue;
            this.NewBtn.BorderRadius = 15;
            this.NewBtn.CheckedState.Parent = this.NewBtn;
            this.NewBtn.CustomImages.Parent = this.NewBtn;
            this.NewBtn.FillColor = System.Drawing.Color.Navy;
            this.NewBtn.Font = new System.Drawing.Font("Segoe UI Symbol", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NewBtn.ForeColor = System.Drawing.Color.White;
            this.NewBtn.HoverState.Parent = this.NewBtn;
            this.NewBtn.Location = new System.Drawing.Point(226, 502);
            this.NewBtn.Name = "NewBtn";
            this.NewBtn.ShadowDecoration.Parent = this.NewBtn;
            this.NewBtn.Size = new System.Drawing.Size(139, 37);
            this.NewBtn.TabIndex = 32;
            this.NewBtn.Text = "NEW";
            this.NewBtn.Click += new System.EventHandler(this.NewBtn_Click);
            // 
            // Closebtn
            // 
            this.Closebtn.BorderColor = System.Drawing.Color.DarkBlue;
            this.Closebtn.BorderRadius = 15;
            this.Closebtn.CheckedState.Parent = this.Closebtn;
            this.Closebtn.CustomImages.Parent = this.Closebtn;
            this.Closebtn.FillColor = System.Drawing.Color.Navy;
            this.Closebtn.Font = new System.Drawing.Font("Segoe UI Symbol", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Closebtn.ForeColor = System.Drawing.Color.White;
            this.Closebtn.HoverState.Parent = this.Closebtn;
            this.Closebtn.Location = new System.Drawing.Point(423, 502);
            this.Closebtn.Name = "Closebtn";
            this.Closebtn.ShadowDecoration.Parent = this.Closebtn;
            this.Closebtn.Size = new System.Drawing.Size(135, 37);
            this.Closebtn.TabIndex = 33;
            this.Closebtn.Text = "CLOSE";
            this.Closebtn.Click += new System.EventHandler(this.Closebtn_Click);
            // 
            // NewCustomerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(626, 591);
            this.Controls.Add(this.Closebtn);
            this.Controls.Add(this.NewBtn);
            this.Controls.Add(this.LoginButton);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.AddressTextBox);
            this.Controls.Add(this.MobileTextBox);
            this.Controls.Add(this.CustomerTextBox);
            this.Name = "NewCustomerForm";
            this.Text = "New Customer";
            this.Load += new System.EventHandler(this.NewCustomerForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox AddressTextBox;
        private System.Windows.Forms.TextBox MobileTextBox;
        private System.Windows.Forms.TextBox CustomerTextBox;
        private Guna.UI2.WinForms.Guna2Button LoginButton;
        private Guna.UI2.WinForms.Guna2Button NewBtn;
        private Guna.UI2.WinForms.Guna2Button Closebtn;
    }
}