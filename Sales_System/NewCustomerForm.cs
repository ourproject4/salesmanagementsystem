﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sales_System
{
    public partial class NewCustomerForm : MetroFramework.Forms.MetroForm
    {
       

        public bool IsUpdate { get; private set; }
        public int CustomerID { get; private set; }

        public NewCustomerForm()
        {
            InitializeComponent();
        }

        private void LoginButton_Click(object sender, EventArgs e)
        {
            if (IsValid())
            {
                if (this.IsUpdate)
                {
                    using (SqlConnection con = new SqlConnection(ApplicationSetting.ConnectionString()))
                    {
                        using (SqlCommand cmd = new SqlCommand("usp_CustomerUpdateByCustomerID", con))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@Name", CustomerTextBox.Text);
                            cmd.Parameters.AddWithValue("@Address", AddressTextBox.Text);
                            cmd.Parameters.AddWithValue("@Mobile", MobileTextBox.Text);
                            cmd.Parameters.AddWithValue("@CustomerID", this.CustomerID);
                            con.Open();
                            int id = Convert.ToInt32(cmd.ExecuteScalar());

                            MessageBox.Show("Customer details updated successfully in the system ", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                }
                else
                {
                    using (SqlConnection con = new SqlConnection(ApplicationSetting.ConnectionString()))
                    {
                        using (SqlCommand cmd = new SqlCommand("usp_Insert_Customers", con))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@Name", CustomerTextBox.Text);
                            cmd.Parameters.AddWithValue("@Address", AddressTextBox.Text);
                            cmd.Parameters.AddWithValue("@Mobile", MobileTextBox.Text);
                            con.Open();
                            int id = Convert.ToInt32(cmd.ExecuteScalar());
                            MessageBox.Show("Customer No. " + id.ToString() + " is saved successfully in the system ", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                }

            }
        }

        private bool IsValid()
        {
            if (CustomerTextBox.Text.Trim() == string.Empty)
            {
                MessageBox.Show("Customer Name is required", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                CustomerTextBox.Focus();
                return false;
            }

            if (AddressTextBox.Text.Trim() == string.Empty)
            {
                MessageBox.Show("Customer Address is required", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                AddressTextBox.Focus();
                return false;
            }

            if (MobileTextBox.Text.Trim() == string.Empty)
            {
                MessageBox.Show("Customer Mobile is required", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                MobileTextBox.Focus();
                return false;
            }
            return true;
        }

        private void NewBtn_Click(object sender, EventArgs e)
        {
            CustomerTextBox.Clear();
            MobileTextBox.Clear();
            AddressTextBox.Clear();

            CustomerTextBox.Focus();
            this.IsUpdate = false;
            this.CustomerID = 0;
        }

        private void Closebtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void NewCustomerForm_Load(object sender, EventArgs e)
        {
            if (this.IsUpdate)
            {
                LoadDataAndMapIntoControls();
            }
        }

        private void LoadDataAndMapIntoControls()
        {
            DataTable dtRecords = GetCustomerData();
            DataRow row;
            row = dtRecords.Rows[0];

            CustomerTextBox.Text = row["Name"].ToString();
            AddressTextBox.Text = row["Address"].ToString();
            MobileTextBox.Text = row["Mobile"].ToString();
        }

        private DataTable GetCustomerData()
        {
            DataTable dtdata = new DataTable();

            using (SqlConnection con = new SqlConnection(ApplicationSetting.ConnectionString()))
            {
                using (SqlCommand cmd = new SqlCommand("usp_CustomerGetCustomerDatabyCustomerID", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@CustomerID", this.CustomerID);
                    con.Open();
                    SqlDataReader sdr = cmd.ExecuteReader();
                    dtdata.Load(sdr);
                }
            }
            return dtdata;
        }
    }
    
}
