﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sales_System
{
    public partial class ProductsRecordsForm : MetroFramework.Forms.MetroForm
    {
        public ProductsRecordsForm()
        {
            InitializeComponent();
        }

        private void ProductsRecordsForm_Load(object sender, EventArgs e)
        {
            LoadAllProductsIntoDataGridView();
        }

        private void LoadAllProductsIntoDataGridView()
        {
            if (ProductsDataGridView.Columns.Contains("Delete"))
            {
                ProductsDataGridView.Columns.Remove("Delete");
            }

            ProductsDataGridView.DataSource = GetData();
            ProductsDataGridView.Columns[0].Visible = true;

            DataGridViewButtonColumn dvc = new DataGridViewButtonColumn
            {
                HeaderText = "Delete",
                Name = "Delete"
            };
            ProductsDataGridView.Columns.Add(dvc);
        }

        private DataTable GetData()
        {
            DataTable dtrecord = new DataTable();

            using (SqlConnection con = new SqlConnection(ApplicationSetting.ConnectionString()))
            {
                using (SqlCommand cmd = new SqlCommand("usp_Products_LoadAllProductsForDataGridView", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    con.Open();
                    SqlDataReader sdr = cmd.ExecuteReader();
                    dtrecord.Load(sdr);
                }
            }
            return dtrecord;
        }

        private void PronameTextBox_TextChanged(object sender, EventArgs e)
        {
            if (PronameTextBox.Text.Trim() == string.Empty)
            {
                LoadAllProductsIntoDataGridView();
                ProductsDataGridView.Columns[0].Visible = true;
            }
            else
            {

                ProductsDataGridView.DataSource = GetProductByProductID();

            }
        }

        private DataTable GetProductByProductID()
        {
            DataTable dtrecord = new DataTable();

            using (SqlConnection con = new SqlConnection(ApplicationSetting.ConnectionString()))
            {
                using (SqlCommand cmd = new SqlCommand("ups_Products_LoadAllProductsbyProductName", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ProductName", PronameTextBox.Text.Trim());
                    con.Open();
                    SqlDataReader sdr = cmd.ExecuteReader();
                    dtrecord.Load(sdr);
                }
            }
            return dtrecord;
        }

        private void ProductsDataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {

            if (ProductsDataGridView.Columns[e.ColumnIndex].HeaderText == "Delete")
            {
                DialogResult confirm = MessageBox.Show("Are you sure you want to delete", "Message", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (confirm == DialogResult.Yes)
                {
                    int ProductId;
                    ProductId = Convert.ToInt32(ProductsDataGridView.Rows[e.RowIndex].Cells["ProductId"].Value);
                    using (SqlConnection con = new SqlConnection(ApplicationSetting.ConnectionString()))
                    {
                        con.Open();
                        try
                        {
                            using (SqlCommand cmd = new SqlCommand("DELETE FROM Products WHERE ProductId=@code", con))
                            {
                                cmd.Parameters.AddWithValue("@code", ProductId);
                                int result = cmd.ExecuteNonQuery();
                                if (result > 0)
                                {
                                    MessageBox.Show("Data Deleted Successfully");
                                }
                                else
                                {
                                    MessageBox.Show("Data not deleted");
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                        finally
                        {
                            con.Close();
                        }

                    }
                }
            }


        }

        private void Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
    

