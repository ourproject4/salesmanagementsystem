﻿
namespace Sales_System
{
    partial class SalesOrderDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.Close = new Guna.UI2.WinForms.Guna2Button();
            this.Addorder = new Guna.UI2.WinForms.Guna2Button();
            this.SalesOrderDataGridView = new MetroFramework.Controls.MetroGrid();
            ((System.ComponentModel.ISupportInitialize)(this.SalesOrderDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Red;
            this.panel1.Location = new System.Drawing.Point(23, 596);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1213, 10);
            this.panel1.TabIndex = 40;
            // 
            // Close
            // 
            this.Close.BorderColor = System.Drawing.Color.MidnightBlue;
            this.Close.BorderRadius = 18;
            this.Close.BorderThickness = 2;
            this.Close.CheckedState.Parent = this.Close;
            this.Close.CustomImages.Parent = this.Close;
            this.Close.FillColor = System.Drawing.Color.Black;
            this.Close.Font = new System.Drawing.Font("Segoe UI Emoji", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Close.ForeColor = System.Drawing.Color.White;
            this.Close.HoverState.Parent = this.Close;
            this.Close.Location = new System.Drawing.Point(1091, 526);
            this.Close.Name = "Close";
            this.Close.ShadowDecoration.Parent = this.Close;
            this.Close.Size = new System.Drawing.Size(143, 38);
            this.Close.TabIndex = 41;
            this.Close.Text = "CLOSE";
            this.Close.Click += new System.EventHandler(this.Close_Click);
            // 
            // Addorder
            // 
            this.Addorder.BackColor = System.Drawing.Color.White;
            this.Addorder.BorderColor = System.Drawing.Color.MidnightBlue;
            this.Addorder.BorderRadius = 18;
            this.Addorder.BorderThickness = 2;
            this.Addorder.CheckedState.Parent = this.Addorder;
            this.Addorder.CustomImages.Parent = this.Addorder;
            this.Addorder.FillColor = System.Drawing.Color.Red;
            this.Addorder.Font = new System.Drawing.Font("Segoe UI Emoji", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Addorder.ForeColor = System.Drawing.Color.White;
            this.Addorder.HoverState.Parent = this.Addorder;
            this.Addorder.Location = new System.Drawing.Point(786, 526);
            this.Addorder.Name = "Addorder";
            this.Addorder.ShadowDecoration.Parent = this.Addorder;
            this.Addorder.Size = new System.Drawing.Size(279, 38);
            this.Addorder.TabIndex = 42;
            this.Addorder.Text = "ADD  ORDER";
            this.Addorder.Click += new System.EventHandler(this.Addorder_Click);
            // 
            // SalesOrderDataGridView
            // 
            this.SalesOrderDataGridView.AllowUserToAddRows = false;
            this.SalesOrderDataGridView.AllowUserToDeleteRows = false;
            this.SalesOrderDataGridView.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.White;
            this.SalesOrderDataGridView.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.SalesOrderDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.SalesOrderDataGridView.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.SalesOrderDataGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.SalesOrderDataGridView.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.SalesOrderDataGridView.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(17)))), ((int)(((byte)(65)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(19)))), ((int)(((byte)(73)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.SalesOrderDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.SalesOrderDataGridView.ColumnHeadersHeight = 25;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(19)))), ((int)(((byte)(73)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.SalesOrderDataGridView.DefaultCellStyle = dataGridViewCellStyle3;
            this.SalesOrderDataGridView.EnableHeadersVisualStyles = false;
            this.SalesOrderDataGridView.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.SalesOrderDataGridView.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.SalesOrderDataGridView.Location = new System.Drawing.Point(21, 99);
            this.SalesOrderDataGridView.Name = "SalesOrderDataGridView";
            this.SalesOrderDataGridView.ReadOnly = true;
            this.SalesOrderDataGridView.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(17)))), ((int)(((byte)(65)))));
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(19)))), ((int)(((byte)(73)))));
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.SalesOrderDataGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.SalesOrderDataGridView.RowHeadersWidth = 51;
            this.SalesOrderDataGridView.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.SalesOrderDataGridView.RowTemplate.Height = 24;
            this.SalesOrderDataGridView.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.SalesOrderDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.SalesOrderDataGridView.Size = new System.Drawing.Size(1215, 389);
            this.SalesOrderDataGridView.Style = MetroFramework.MetroColorStyle.Red;
            this.SalesOrderDataGridView.TabIndex = 43;
            this.SalesOrderDataGridView.Theme = MetroFramework.MetroThemeStyle.Light;
            this.SalesOrderDataGridView.UseCustomBackColor = true;
            this.SalesOrderDataGridView.UseCustomForeColor = true;
            this.SalesOrderDataGridView.UseStyleColors = true;
            // 
            // SalesOrderDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1257, 642);
            this.Controls.Add(this.SalesOrderDataGridView);
            this.Controls.Add(this.Addorder);
            this.Controls.Add(this.Close);
            this.Controls.Add(this.panel1);
            this.Name = "SalesOrderDetails";
            this.Style = MetroFramework.MetroColorStyle.Orange;
            this.Text = "SalesOrderDetails";
            this.Load += new System.EventHandler(this.SalesOrderDetails_Load);
            ((System.ComponentModel.ISupportInitialize)(this.SalesOrderDataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel1;
        private Guna.UI2.WinForms.Guna2Button Close;
        private Guna.UI2.WinForms.Guna2Button Addorder;
        private MetroFramework.Controls.MetroGrid SalesOrderDataGridView;
    }
}