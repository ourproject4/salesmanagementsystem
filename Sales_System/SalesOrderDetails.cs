﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sales_System
{
    public partial class SalesOrderDetails : MetroFramework.Forms.MetroForm
    {
        public SalesOrderDetails()
        {
            InitializeComponent();
        }

        private void SalesOrderDetails_Load(object sender, EventArgs e)
        {
            LoadAllAddSalesOrderData();
        }

        private void LoadAllAddSalesOrderData()
        {
            SalesOrderDataGridView.DataSource = GetData();
            SalesOrderDataGridView.Columns[0].Visible = true;
        }

        private DataTable GetData()
        {
            DataTable salesOrderdata = new DataTable();
            using (SqlConnection con = new SqlConnection(ApplicationSetting.ConnectionString()))
            {
                using (SqlCommand cmd = new SqlCommand("Select * from SalesOrder", con))
                {

                    con.Open();
                    SqlDataReader sdr = cmd.ExecuteReader();
                    salesOrderdata.Load(sdr);
                }
            }
            return salesOrderdata;
        }

        private void Addorder_Click(object sender, EventArgs e)
        {
            AddSalesOrder aso = new AddSalesOrder();
            aso.Show();
        }

        private void Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
