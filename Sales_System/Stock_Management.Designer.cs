﻿
namespace Sales_System
{
    partial class Stock_Management
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            this.DateTimePickerBox = new System.Windows.Forms.DateTimePicker();
            this.ProNameTextBox = new System.Windows.Forms.TextBox();
            this.ProductIDTextBox = new System.Windows.Forms.TextBox();
            this.SalesPriceTextBox = new System.Windows.Forms.TextBox();
            this.QuantityTextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.StocksDataGridView = new MetroFramework.Controls.MetroGrid();
            this.SaveBtn = new Guna.UI2.WinForms.Guna2Button();
            this.Clearbtn = new Guna.UI2.WinForms.Guna2Button();
            this.Closebtn = new Guna.UI2.WinForms.Guna2Button();
            ((System.ComponentModel.ISupportInitialize)(this.StocksDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // DateTimePickerBox
            // 
            this.DateTimePickerBox.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DateTimePickerBox.Location = new System.Drawing.Point(33, 193);
            this.DateTimePickerBox.Name = "DateTimePickerBox";
            this.DateTimePickerBox.Size = new System.Drawing.Size(97, 22);
            this.DateTimePickerBox.TabIndex = 0;
            // 
            // ProNameTextBox
            // 
            this.ProNameTextBox.Font = new System.Drawing.Font("Segoe UI Emoji", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ProNameTextBox.Location = new System.Drawing.Point(334, 193);
            this.ProNameTextBox.Multiline = true;
            this.ProNameTextBox.Name = "ProNameTextBox";
            this.ProNameTextBox.Size = new System.Drawing.Size(169, 31);
            this.ProNameTextBox.TabIndex = 24;
            this.ProNameTextBox.TextChanged += new System.EventHandler(this.ProNameTextBox_TextChanged);
            // 
            // ProductIDTextBox
            // 
            this.ProductIDTextBox.Font = new System.Drawing.Font("Segoe UI Emoji", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ProductIDTextBox.Location = new System.Drawing.Point(161, 193);
            this.ProductIDTextBox.Multiline = true;
            this.ProductIDTextBox.Name = "ProductIDTextBox";
            this.ProductIDTextBox.Size = new System.Drawing.Size(145, 31);
            this.ProductIDTextBox.TabIndex = 25;
            this.ProductIDTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ProductIDTextBox_KeyPress);
            // 
            // SalesPriceTextBox
            // 
            this.SalesPriceTextBox.Font = new System.Drawing.Font("Segoe UI Emoji", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SalesPriceTextBox.Location = new System.Drawing.Point(539, 193);
            this.SalesPriceTextBox.Multiline = true;
            this.SalesPriceTextBox.Name = "SalesPriceTextBox";
            this.SalesPriceTextBox.Size = new System.Drawing.Size(169, 31);
            this.SalesPriceTextBox.TabIndex = 26;
            this.SalesPriceTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.SalesPriceTextBox_KeyPress);
            // 
            // QuantityTextBox
            // 
            this.QuantityTextBox.Font = new System.Drawing.Font("Segoe UI Emoji", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.QuantityTextBox.Location = new System.Drawing.Point(750, 193);
            this.QuantityTextBox.Multiline = true;
            this.QuantityTextBox.Name = "QuantityTextBox";
            this.QuantityTextBox.Size = new System.Drawing.Size(169, 31);
            this.QuantityTextBox.TabIndex = 27;
            this.QuantityTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.QuantityTextBox_KeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI Symbol", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(29, 146);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(50, 23);
            this.label5.TabIndex = 31;
            this.label5.Text = "Date";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI Symbol", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(157, 146);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(96, 23);
            this.label2.TabIndex = 33;
            this.label2.Text = "ProductID";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI Symbol", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(330, 146);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(133, 23);
            this.label3.TabIndex = 34;
            this.label3.Text = "Product Name";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI Symbol", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(535, 146);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(101, 23);
            this.label4.TabIndex = 35;
            this.label4.Text = "Sales Price";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe UI Symbol", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(746, 146);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(84, 23);
            this.label6.TabIndex = 36;
            this.label6.Text = "Quantity";
            // 
            // StocksDataGridView
            // 
            this.StocksDataGridView.AllowUserToAddRows = false;
            this.StocksDataGridView.AllowUserToDeleteRows = false;
            this.StocksDataGridView.AllowUserToResizeRows = false;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black;
            this.StocksDataGridView.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle5;
            this.StocksDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.StocksDataGridView.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.StocksDataGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.StocksDataGridView.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.StocksDataGridView.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(85)))), ((int)(((byte)(85)))));
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.StocksDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.StocksDataGridView.ColumnHeadersHeight = 25;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.StocksDataGridView.DefaultCellStyle = dataGridViewCellStyle7;
            this.StocksDataGridView.EnableHeadersVisualStyles = false;
            this.StocksDataGridView.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.StocksDataGridView.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.StocksDataGridView.Location = new System.Drawing.Point(23, 349);
            this.StocksDataGridView.Name = "StocksDataGridView";
            this.StocksDataGridView.ReadOnly = true;
            this.StocksDataGridView.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(85)))), ((int)(((byte)(85)))));
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.StocksDataGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.StocksDataGridView.RowHeadersWidth = 51;
            this.StocksDataGridView.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.StocksDataGridView.RowTemplate.Height = 24;
            this.StocksDataGridView.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.StocksDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.StocksDataGridView.Size = new System.Drawing.Size(1109, 410);
            this.StocksDataGridView.Style = MetroFramework.MetroColorStyle.Silver;
            this.StocksDataGridView.TabIndex = 37;
            this.StocksDataGridView.Theme = MetroFramework.MetroThemeStyle.Light;
            this.StocksDataGridView.UseCustomBackColor = true;
            this.StocksDataGridView.UseCustomForeColor = true;
            this.StocksDataGridView.UseStyleColors = true;
            this.StocksDataGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.StocksDataGridView_CellClick);
            // 
            // SaveBtn
            // 
            this.SaveBtn.BorderColor = System.Drawing.Color.MidnightBlue;
            this.SaveBtn.BorderRadius = 18;
            this.SaveBtn.BorderThickness = 2;
            this.SaveBtn.CheckedState.Parent = this.SaveBtn;
            this.SaveBtn.CustomImages.Parent = this.SaveBtn;
            this.SaveBtn.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.SaveBtn.Font = new System.Drawing.Font("Segoe UI Emoji", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SaveBtn.ForeColor = System.Drawing.Color.White;
            this.SaveBtn.HoverState.Parent = this.SaveBtn;
            this.SaveBtn.Location = new System.Drawing.Point(129, 266);
            this.SaveBtn.Name = "SaveBtn";
            this.SaveBtn.ShadowDecoration.Parent = this.SaveBtn;
            this.SaveBtn.Size = new System.Drawing.Size(128, 41);
            this.SaveBtn.TabIndex = 38;
            this.SaveBtn.Text = "SAVE";
            this.SaveBtn.Click += new System.EventHandler(this.SaveBtn_Click);
            // 
            // Clearbtn
            // 
            this.Clearbtn.BorderColor = System.Drawing.Color.MidnightBlue;
            this.Clearbtn.BorderRadius = 18;
            this.Clearbtn.BorderThickness = 2;
            this.Clearbtn.CheckedState.Parent = this.Clearbtn;
            this.Clearbtn.CustomImages.Parent = this.Clearbtn;
            this.Clearbtn.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.Clearbtn.Font = new System.Drawing.Font("Segoe UI Emoji", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Clearbtn.ForeColor = System.Drawing.Color.White;
            this.Clearbtn.HoverState.Parent = this.Clearbtn;
            this.Clearbtn.Location = new System.Drawing.Point(317, 266);
            this.Clearbtn.Name = "Clearbtn";
            this.Clearbtn.ShadowDecoration.Parent = this.Clearbtn;
            this.Clearbtn.Size = new System.Drawing.Size(134, 42);
            this.Clearbtn.TabIndex = 39;
            this.Clearbtn.Text = "CLEAR";
            this.Clearbtn.Click += new System.EventHandler(this.Clearbtn_Click);
            // 
            // Closebtn
            // 
            this.Closebtn.BorderColor = System.Drawing.Color.MidnightBlue;
            this.Closebtn.BorderRadius = 18;
            this.Closebtn.BorderThickness = 2;
            this.Closebtn.CheckedState.Parent = this.Closebtn;
            this.Closebtn.CustomImages.Parent = this.Closebtn;
            this.Closebtn.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.Closebtn.Font = new System.Drawing.Font("Segoe UI Emoji", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Closebtn.ForeColor = System.Drawing.Color.White;
            this.Closebtn.HoverState.Parent = this.Closebtn;
            this.Closebtn.Location = new System.Drawing.Point(516, 265);
            this.Closebtn.Name = "Closebtn";
            this.Closebtn.ShadowDecoration.Parent = this.Closebtn;
            this.Closebtn.Size = new System.Drawing.Size(134, 42);
            this.Closebtn.TabIndex = 40;
            this.Closebtn.Text = "CLOSE";
            this.Closebtn.Click += new System.EventHandler(this.Closebtn_Click);
            // 
            // Stock_Management
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1155, 781);
            this.Controls.Add(this.Closebtn);
            this.Controls.Add(this.Clearbtn);
            this.Controls.Add(this.SaveBtn);
            this.Controls.Add(this.StocksDataGridView);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.QuantityTextBox);
            this.Controls.Add(this.SalesPriceTextBox);
            this.Controls.Add(this.ProductIDTextBox);
            this.Controls.Add(this.ProNameTextBox);
            this.Controls.Add(this.DateTimePickerBox);
            this.Name = "Stock_Management";
            this.Style = MetroFramework.MetroColorStyle.Purple;
            this.Text = "Stock_Management";
            this.Load += new System.EventHandler(this.Stock_Management_Load);
            ((System.ComponentModel.ISupportInitialize)(this.StocksDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker DateTimePickerBox;
        private System.Windows.Forms.TextBox ProNameTextBox;
        private System.Windows.Forms.TextBox ProductIDTextBox;
        private System.Windows.Forms.TextBox SalesPriceTextBox;
        private System.Windows.Forms.TextBox QuantityTextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private MetroFramework.Controls.MetroGrid StocksDataGridView;
        private Guna.UI2.WinForms.Guna2Button SaveBtn;
        private Guna.UI2.WinForms.Guna2Button Clearbtn;
        private Guna.UI2.WinForms.Guna2Button Closebtn;
    }
}