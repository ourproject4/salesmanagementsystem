﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sales_System
{
    public partial class Stock_Management : MetroFramework.Forms.MetroForm
    {
        public Stock_Management()
        {
            InitializeComponent();
        }

        private void Stock_Management_Load(object sender, EventArgs e)
        {
            LoadAllStocksData();
            this.ActiveControl = DateTimePickerBox;
           
        }

        private void LoadAllStocksData()
        {
            StocksDataGridView.DataSource = GetData();
            StocksDataGridView.Columns[0].Visible = true;

            if (StocksDataGridView.Columns.Contains("Delete"))
            {
                StocksDataGridView.Columns.Remove("Delete");
            }

            StocksDataGridView.DataSource = GetData();
            StocksDataGridView.Columns[0].Visible = true;

            DataGridViewButtonColumn dvc = new DataGridViewButtonColumn
            {
                HeaderText = "Delete",
                Name = "Delete"
            };
            StocksDataGridView.Columns.Add(dvc);
        }

        private DataTable GetData()
        {
            DataTable stockdata = new DataTable();
            using (SqlConnection con = new SqlConnection(ApplicationSetting.ConnectionString()))
            {
                using (SqlCommand cmd = new SqlCommand("Select * from Stock", con))
                {

                    con.Open();
                    SqlDataReader sdr = cmd.ExecuteReader();
                    stockdata.Load(sdr);
                }
            }
            return stockdata;
        }

        private void ProNameTextBox_TextChanged(object sender, EventArgs e)
        {
            if (ProNameTextBox.Text.Trim() == string.Empty)
            {
                LoadAllStocksData();
            }
            else
            {
                StocksDataGridView.DataSource = GetData();
                StocksDataGridView.Columns[0].Visible = true;
            }
        }

        private void StockIDTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsNumber(e.KeyChar) & (Keys)e.KeyChar != Keys.Back & e.KeyChar != '.')
            {
                e.Handled = true;
            }
        }

        private void ProductIDTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsNumber(e.KeyChar) & (Keys)e.KeyChar != Keys.Back & e.KeyChar != '.')
            {
                e.Handled = true;
            }
        }

        private void SalesPriceTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsNumber(e.KeyChar) & (Keys)e.KeyChar != Keys.Back & e.KeyChar != '.')
            {
                e.Handled = true;
            }
        }

        private void QuantityTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsNumber(e.KeyChar) & (Keys)e.KeyChar != Keys.Back & e.KeyChar != '.')
            {
                e.Handled = true;
            }
        }

        private void Clearbtn_Click(object sender, EventArgs e)
        {
            ResetAllRecords();
        }

        private void ResetAllRecords()
        {
            DateTimePickerBox.Value = DateTime.Now;
            ProductIDTextBox.Clear();
            ProNameTextBox.Clear();
            SalesPriceTextBox.Clear();
            QuantityTextBox.Clear();
           
            SaveBtn.Text = "Add";
            DateTimePickerBox.Focus();
            DateTimePickerBox.Value = DateTime.Now;
        }

        private void Closebtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void SaveBtn_Click(object sender, EventArgs e)
        {
            if (IsValid())
            {
                using (SqlConnection con = new SqlConnection(ApplicationSetting.ConnectionString()))
                {
                    using (SqlCommand cmd = new SqlCommand("usp_Add_Stocks", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        
                        cmd.Parameters.AddWithValue("@ProductId", ProductIDTextBox.Text);
                        cmd.Parameters.AddWithValue("@ProductName", ProNameTextBox.Text);
                        cmd.Parameters.AddWithValue("@Date", DateTimePickerBox.Value);
                        cmd.Parameters.AddWithValue("@SalesPrice", SalesPriceTextBox.Text);
                        
                        cmd.Parameters.AddWithValue("@Quantity", QuantityTextBox.Text);
                        con.Open();
                        int id = Convert.ToInt32(cmd.ExecuteScalar());
                        MessageBox.Show("StockID." + id.ToString() + " successfully saved in the system ", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    }
                }

            }

        }
        private bool IsValid()
        {
            

            if (ProductIDTextBox.Text.Trim() == string.Empty)
            {
                MessageBox.Show("ProductID is required", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                ProductIDTextBox.Focus();
                return false;
            }

            if (ProNameTextBox.Text.Trim() == string.Empty)
            {
                MessageBox.Show("Product Name is required", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                ProNameTextBox.Focus();
                return false;
            }
            if (SalesPriceTextBox.Text.Trim() == string.Empty)
            {
                MessageBox.Show("SalesPrice is required", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                SalesPriceTextBox.Focus();
                return false;
            }
            if (QuantityTextBox.Text.Trim() == string.Empty)
            {
                MessageBox.Show("Quantity is required", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                QuantityTextBox.Focus();
                return false;
            }
            return true;

        }

        private void StocksDataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (StocksDataGridView.Columns[e.ColumnIndex].HeaderText == "Delete")
            {
                DialogResult confirm = MessageBox.Show("Are you sure you want to delete", "Message", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (confirm == DialogResult.Yes)
                {
                    int ProductId;
                    ProductId = Convert.ToInt32(StocksDataGridView.Rows[e.RowIndex].Cells["ProductId"].Value);
                    using (SqlConnection con = new SqlConnection(ApplicationSetting.ConnectionString()))
                    {
                        con.Open();
                        try
                        {
                            using (SqlCommand cmd = new SqlCommand("DELETE FROM Products WHERE ProductId=@code", con))
                            {
                                cmd.Parameters.AddWithValue("@code", ProductId);
                                int result = cmd.ExecuteNonQuery();
                                if (result > 0)
                                {
                                    MessageBox.Show("Data Deleted Successfully");
                                }
                                else
                                {
                                    MessageBox.Show("Data not deleted");
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                        finally
                        {
                            con.Close();
                        }

                    }
                }
            }
        }

    }
}
    
